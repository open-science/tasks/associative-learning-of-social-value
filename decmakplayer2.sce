 # Associative learning of social value paradigm
 # 
 # Player 2 task - scenario file
 #
 # Laurence Hunt and Tim Behrens, 2007

 scenario="Associative learning of social value";
 scenario_type=fMRI_emulation;
 pulses_per_scan=1;
 pulse_code=33; 
 scan_period=6000;
 pcl_file = "decmakplayer2.pcl" ;
 active_buttons=2;
 button_codes=1,2; 
 response_matching = simple_matching;
 no_logfile=true;
 begin;    
 
 
 text {caption="0";
      font_size=24;
      font = "Arial";
       font_color = 255,255,0;
    preload=false;} money_text;
 
  text {caption="85";
      font_size=32;
      font = "Arial";
       font_color = 255,255,0;
      } lefttext;
  
   text {caption="15";
      font_size=32;
      font = "Arial";
       font_color = 255,255,0;
     } righttext;
     
   text {caption="Please choose advice to give to partner:";
      font_size=20;
      font = "Times New Roman";
       font_color = 255,255,255;
     } instructiontext;
     
   text {caption="Give correct answer";
      font_size=16;
      font = "Times New Roman";
       font_color = 255,255,255;
     } correcttext;
     
   text {caption="Give correct answer";
      font_size=16;
      font = "Times New Roman";
       font_color = 255,0,0;
     } correcttextred;
     
   text {caption="Give incorrect answer";
     font_size=16;
     font = "Times New Roman";
      font_color = 255,255,255;
    } incorrecttext;
    
   text {caption="Give incorrect answer";
     font_size=16;
     font = "Times New Roman";
      font_color = 255,0,0;
    } incorrecttextred;
     
   text {caption="Awiting response from partner.";
      font_size=20;
      font = "Times New Roman";
       font_color = 255,255,255;
     } responsetext;
     
   text {caption="Response given.";
      font_size=20;
      font = "Times New Roman";
       font_color = 255,255,255;
     } completetext;

   text {caption="Partner's current score:";
      font_size=24;
      font = "Times New Roman";
       font_color = 255,255,255;
     } partnerscoretext;

   text {caption="Round x of y:";
      font_size=24;
      font = "Times New Roman";
       font_color = 255,255,255;
     } trialnumbertext;            
     
box {
    height = 10;
    width = 80;
    color = 169,169,169;
} silverrect;

box {
    height = 5;
    width = 2;
    color = 169,169,169;
} silverdash;

text {caption="�10";
   font_size=14;
   font = "Arial";
   font_color = 169, 169, 169;
} silvertext;  
     
text {caption="�20";
    font_size=14;
    font = "Arial";
    font_color = 218, 165, 32;
} goldtext;

box {
    height = 5;
    width = 2;
    color = 218,165,32;
} golddash;  
 
  box {
    height = 10;
    width = 60;
    color = 218, 165, 32;
} goldrect;

box {
    height = 250;
    width = 187;
    color = 255, 0, 0;
} redrect;
 
 box {
    height = 250;
    width = 187;
    color = 0, 0, 0;
} blackrect;
     
  box {
    height = 250;
    width = 187;
    color = 128, 128, 128;
} greyrect;
 box {
    height = 30;
    width = 10;
    color = 255,0,0;
} moneybar;    
 
 box {
    height = 30;
    width = 10;
    color = 169,169,169;
} target1;    
 
 box {
    height = 30;
    width = 10;
    color = 218, 165, 32;
} target2;    
  
              
 picture{}default;
       
 
                 
trial{    
   trial_type = first_response;
   trial_duration = forever;   
   stimulus_event{
     picture{   
       text instructiontext; x=0;y=100;
       text correcttext; x=300;y=0;
       text incorrecttext; x=-300;y=0;
       text trialnumbertext; x=0; y=300;
     } picture1; 
     stimulus_time_in = 100;     
     stimulus_time_out = never;     
     code = "advice";
   } ste1;
}cue;

trial{    
   trial_duration = 6000;   
   stimulus_event{
   picture{ 
       text instructiontext; x=0;y=100;
       text correcttextred; x=300;y=0;
       text incorrecttext; x=-300;y=0;
       text trialnumbertext; x=0; y=300;
          } suggestpicture; 
   code = "suggest";
   } ste_event;
}suggest;


   trial{                  
      trial_duration = 1500;
      stimulus_event{
      picture{ 
       text instructiontext; x=0;y=100;
       text correcttextred; x=300;y=0;
       text incorrecttext; x=-300;y=0;
       text responsetext; x=0; y=-200;
       text trialnumbertext; x=0; y=300;        
      } picture2;  
      code = "response";   
      }ste2;   
   } resp;
     
     
  trial{    
   trial_duration = 6000;   
   stimulus_event{
   picture{ 
       text instructiontext; x=0;y=100;
       text correcttextred; x=300;y=0;
       text incorrecttext; x=-300;y=0;
       text completetext; x=0; y=-200;
       text trialnumbertext; x=0; y=300;
          } picture3; 
   code = "select";
   } ste_sel;
}select;

     
     
   trial{  
      trial_duration = 2000;  
   stimulus_event{   
        picture {
         text partnerscoretext; x=0; y=100;              
         box target1;left_x=100;y=0;
         box target2;left_x=300;y=0;
         box moneybar;left_x=-400;y=0;
         text silvertext; x=-85;y=-50;
         #silver zone:
         box silverrect; left_x=-125;y=-15;
         box silverdash;left_x=-125;bottom_y=-10;
         box silverdash;left_x=-125;bottom_y=0;
         box silverdash;left_x=-125;bottom_y=10;
         box silverdash;right_x=-45;bottom_y=-10;
         box silverdash;right_x=-45;bottom_y=0; 
         box silverdash;right_x=-45;bottom_y=10;
         # gold zone:
         text goldtext; x=30;y=-50;
         box goldrect; left_x=0;y=-15;
         box golddash;left_x=0;bottom_y=-10;
         box golddash;left_x=0;bottom_y=0;    
         box golddash;left_x=0;bottom_y=10;
         box golddash;right_x=60;bottom_y=-10;  
         box golddash;right_x=60;bottom_y=0;  
         box golddash;right_x=60;bottom_y=10;           
         } feedbackpic;
			code="feedback";	
	  }ste3;
   }feedback;    
   
	trial{  
      trial_duration = 5000;  
   stimulus_event{   
        picture {      
         } fixatepic;
			code="fixate";	
	  }ste4;
   }fixate;    
  