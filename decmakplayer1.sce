 # Associative learning of social value paradigm
 # 
 # Player 1 task - scenario file
 #
 # Laurence Hunt and Tim Behrens, 2007

 scenario="Associative learning of social value";
 scenario_type=fMRI_emulation;
# scenario_type=fMRI;
 pulses_per_scan=1;
 pulse_code=33; 
 scan_period=6000;
 pcl_file = "decmakplayer1.pcl" ;
 active_buttons=2;
 button_codes=1,2; 
 response_matching = simple_matching;
 begin;       
 
 
 
 #Object definitions start here
 
  text {caption="85";
      font_size=28;
      font = "Arial";
       font_color = 255,255,0;
      } lefttext;
  
   text {caption="15";
      font_size=28;
      font = "Arial";
       font_color = 255,255,0;
     } righttext;
     
   text {caption="Connecting to partner's computer...please wait.";
      font_size=18;
      font = "Times New Roman";
       font_color = 255,255,255;
     } connectingtext;
     
   text {caption="Connected. Experiment will begin shortly.";
      font_size=18;
      font = "Times New Roman";
       font_color = 255,255,255;
     } connectedtext;
     
	box {
		 height = 150;
		 width = 112;
		 color = 0, 255, 0;
	} greenrect;
	 
	  box {
		 height = 150;
		 width = 112;
		 color = 0, 0, 255;
	} bluerect;
	
	box {
		 height = 187;
		 width = 140;
		 color = 255, 0, 0;
	} redrect;
	 
	 box {
		 height = 187;
		 width = 140;
		 color = 0, 0, 0;
	} blackrect;
     
	 box {
		 height = 187;
		 width = 140;
		 color = 128, 128, 128;
	} greyrect;
	
	 box {
		 height = 25;
		 width = 10;
		 color = 255,0,0;
	} moneybar;    
	 
	 box {
		 height = 25;
		 width = 10;
		 color = 169,169,169;
	} target1;    
	 
	 box {
		 height = 25;
		 width = 10;
		 color = 218, 165, 32;
	} target2;    
	  
	bitmap{filename="images/greyspot1.bmp"; preload=false;} spot;
	bitmap{filename="images/quest.bmp";preload=false;} quest;
              
	picture{}default;
 
       
	trial{    
		trial_duration = 30000;   
		stimulus_event{
		picture{ 
			text connectingtext; x=0; y=0;
			}picture0;
		} connectingpicture;
	}connecting;
	
	trial{    
		trial_duration = 3500;   
		stimulus_event{
		picture{ 
			text connectedtext; x=0; y=0;
			}picture00;
		} connectedpicture;
	}connected;
	
	trial{
		trial_duration = 2000;
		stimulus_event{
		picture{
		}blankpicture;
	 }blankevent;
	}blankscreen; 
						  
	trial{    
		trial_duration = 6000;   
		stimulus_event{
		picture{ 
				box greenrect; x=-200; y=0;  
				bitmap spot; x=0;y=0;   
				box bluerect; x=200; y=0; 
				text lefttext; x=-200;y=0;  
				text righttext; x=200;y=0;        
				box target1;left_x=50;y=-150;
				box target2;left_x=170;y=-150;
				box moneybar;left_x=-250;y=-150;  
				 } picture1;  
		code = "cue";
		} ste1;
	}cue;
	
	trial{    
		trial_duration = 6000;   
		stimulus_event{
		picture{ 
				box blackrect; x=-200; y=0;          
				box greenrect; x=-200; y=0;  
				bitmap spot; x=0;y=0;
				box blackrect; x=200; y=0;   
				box bluerect; x=200; y=0; 
				text lefttext; x=-200;y=0;  
				text righttext; x=200;y=0;
				box target1;left_x=50;y=-150;
				box target2;left_x=170;y=-150;
				box moneybar;left_x=-250;y=-150;
				 } suggestpicture; 
		code = "suggest";
		} ste_event;
	}suggest;


   trial{                  
      trial_type = first_response;
      trial_duration = forever;
      stimulus_event{
      picture{ 
            box blackrect; x=-200; y=0;  
            box greenrect; x=-200; y=0;
            bitmap quest; x=0;y=0;   
            box blackrect; x=200; y=0;  
            box bluerect; x=200; y=0;
            text lefttext; x=-200;y=0;  
            text righttext; x=200;y=0;  
            box target1;left_x=50;y=-150;
            box target2;left_x=170;y=-150;
            box moneybar;left_x=-250;y=-150;
      } picture2;  
      stimulus_time_in = 50;     
      stimulus_time_out = never; 
      code = "response";   
      }ste2;   
   } resp;
     
     
  trial{    
   trial_duration = 6000;   
   stimulus_event{
   picture{ 
         box blackrect; x=-200; y=0;  
         box greenrect; x=-200;y=0;
         bitmap quest; x=0;y=0;   
         box blackrect;x=200;y=0;
         box bluerect; x=200;y=0;
         text lefttext; x=-200;y=0;  
         text righttext; x=200;y=0;  
         box target1;left_x=50;y=-150;
         box target2;left_x=170;y=-150;
         box moneybar;left_x=-250;y=-150;
         } picture3; 
		code = "select";
		} ste_sel;
	}select;

     
     
   trial{  
      trial_duration = 3000;  
   stimulus_event{   
        picture { 
         box blackrect; x=-200; y=0;  
         box greenrect; x=-200;y=0; 
         box greenrect;x=0;y=0;
         box blackrect;x=200;y=0; 
         box bluerect; x=200;y=0;
         text lefttext; x=-200;y=0;  
         text righttext; x=200;y=0;
         box target1;left_x=50;y=-150;
         box target2;left_x=170;y=-150;
         box moneybar;left_x=-250;y=-150;
         } feedbackpic;
			code="feedback";	
	  }ste3;
   }feedback;    
   
	trial{  
      trial_duration = 5000;  
   stimulus_event{   
        picture {   
         bitmap spot; x=0;y=0;    
         box target1;left_x=50;y=-150;
         box target2;left_x=170;y=-150;
         box moneybar;left_x=-250;y=-150;
         } fixatepic;
			code="fixate";	
	  }ste4;
   }fixate;    
  